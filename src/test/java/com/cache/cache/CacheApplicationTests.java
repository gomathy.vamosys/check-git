package com.cache.cache;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;

@SpringBootTest(classes = CacheApplication.class)
class CacheApplicationTests  extends AbstractTestNGSpringContextTests{

	@Autowired
	private WebApplicationContext webApplContext;
	
	private MockMvc mockmvc ;
	
	@BeforeClass
	public void setup() {
		mockmvc = MockMvcBuilders.webAppContextSetup(webApplContext).build();
	}
	
	
	@Test
	void contextLoads() throws Exception {
		System.out.println(" print ");
		mockmvc.perform(MockMvcRequestBuilders.get("/cities"))
					.andExpect(MockMvcResultMatchers.status().isOk());
	}

}
