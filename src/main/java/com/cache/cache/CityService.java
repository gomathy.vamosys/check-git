package com.cache.cache;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityService {

	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private CityRepository cityRepository;

	public City getCityById(Long id){  
		return cityRepository.findById(id).get();  
	} 
	
	public City getCityByPopulation(Long id){  
		
		City ent = em.unwrap(Session.class).byNaturalId(City.class).using("population", id).load();
		
		return ent;
	} 
	
	public City getCityByPopulationV2(Long id){  
		
		City ent =cityRepository.findByPopulation(id);
		
		return ent;
	} 

	public City saveCity(City city){  
		return cityRepository.save(city);  
	} 
}
